#Copyright (c) 2023 Synopsys, Inc. All rights reserved worldwide.
#!/bin/bash

getAllAvailableBridgeVersions() {
    #store html result from api in artifactoryResults temp file
    curl -X GET --header "Accept: text/html" "https://artifactory.internal.synopsys.com/artifactory/clops-local/clops.sig.synopsys.com/synopsys-action/" >> artifactoryResults.html
    IFS=$'\n'
    versionArray=($(sed -n 's/.*href="\([0-9.^"]*\).*/\1/p' artifactoryResults.html))
}

validateBridgeVersion() {
    for i in "${versionArray[@]}"
do
    if [[ $i != "" && $i != ".." ]]; then
        if [[ $i == $inputBridgeVersion ]];then    
            echo "Valid Bridge Version"
            return 0
        fi
    fi    
done  
    return 1
}

getLatestVersion() {
    extractVersions=( $(printf "%s\n" ${versionArray[@]} | sort -V) )
    typeset -p extractVersions
    for (( i=${#extractVersions[@]}-1 ; i>=0 ; i-- )) ; do
        if [[ $i != "" && $i != ".." ]]; then
            latestVersion="${extractVersions[i]}"
            break
        fi
    done
}

downloadBridge() {
    echo "$bridgeDownloadUrl"
    if [[ "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" ]]; then
        Invoke-WebRequest -Uri $bridgeDownloadUrl -OutFile bridge.zip
        tar -xf bridge.zip
    else 
        curl -L $bridgeDownloadUrl -o bridge.zip
        unzip bridge.zip
        chmod +x bridge
    fi    
}

removeTempFiles() {
    rm -rf artifactoryResults.html
}

if [[ $1 == *"https://"* || $1 == *"http://"* ]]; then
    inputBridgeUrl=$1
else 
    inputBridgeVersion=$1
fi

if [[ "$OSTYPE" == "linux"* ]]; then
    platform="linux64"
elif [[ "$OSTYPE" == "darwin"* ]]; then
    platform="macosx"
elif [[ "$OSTYPE" == "cygwin" || "$OSTYPE" == "msys" ]]; then
    platform="win64"
fi

#get all available bridge versions
getAllAvailableBridgeVersions

if [[ $inputBridgeVersion != "" ]]; then
#validateBridgeVersion
    if validateBridgeVersion "$versionArray" "$inputBridgeVersion";then
        bridgeDownloadUrl="https://artifactory.internal.synopsys.com/artifactory/clops-local/clops.sig.synopsys.com/synopsys-action/"$inputBridgeVersion"/ci-package-"$inputBridgeVersion"-"$platform".zip"
        downloadBridge "$bridgeDownloadUrl"
    else 
        echo "Provided bridge version not found in artifactory";
        removeTempFiles
        exit 1
    fi
elif [[ $inputBridgeUrl != "" ]]; then
        bridgeDownloadUrl=$inputBridgeUrl
        downloadBridge "$bridgeDownloadUrl"
else
    getLatestVersion $versionArray   
    echo "valid bridge version $latestVersion";  
    bridgeDownloadUrl="https://artifactory.internal.synopsys.com/artifactory/clops-local/clops.sig.synopsys.com/synopsys-action/"$latestVersion"/ci-package-"$latestVersion"-"$platform".zip"
    downloadBridge "$bridgeDownloadUrl"
fi

#remove artifactoryResults temp file
removeTempFiles
exit 0